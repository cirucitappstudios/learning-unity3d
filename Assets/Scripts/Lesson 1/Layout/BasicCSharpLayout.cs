﻿//Add more code to the already massive amount of code
using UnityEngine;

//Namespaces are like areas of code and is better for bigger projects
namespace Lesson_1.Layout
{
	public class BasicCSharpLayout : MonoBehaviour
	{

		//Comments

		/*
		  Multi
		  Line
		  Comment
		 */

		//Public anything can be used outside of the class but an instance of this class has to be made
		public string PublicString;

		//Private anything can only be used in the class it is declared in
		private string _privateString;

		//If you do not put a public or private "tag" on it then the code will auto make it private
		string _privateString2;

		//Static - Doesn't Require Instance of this class to be used outside of this code
		public static string PublicStaticString;

		//String - Can hold text
		private string _aString;
		
		//Int - Can hold a 32-bit integer 
		private int _maxInt = 2147483647;
		
		//Float - Can hold decimals but is precise up to 7 digits and ends with a 'f' (Example Below - First 7 Digits of PI)
		private float _maxFloat = 3.141592f;
		
		//Bool - True or False
		private bool _bool = true;
		
		//Double - Can hold decimals but is precise up to 16 digits and ends with a 'd' (Example Below - First 16 Digits of PI)
		private double _maxDouble = 3.141592653589793d;
		
		//Long - Can hold a 64-bit integer
		private long _maxLong = 9223372036854775807;

		//Unity3D Method - Loads when code is loaded
		private void Start()
		{
			PublicString = "Hello World";
			PublicStaticString = "I'm Being Used.";
			
			Debug.Log(PublicString + PublicStaticString);
			
		}

		void Update()
		{

		}
	}
}